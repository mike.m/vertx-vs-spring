package com.demo.vertx

import io.vertx.core.Vertx
import io.vertx.core.logging.LoggerFactory

val log = LoggerFactory.getLogger("log")

fun main(args: Array<String>) {
    log.info("Start")
    val v = Vertx.vertx()
    v.deployVerticle(ControllerVerticle::class.java.name)
    println("App started")
}
