package com.demo.vertx

import io.vertx.core.AbstractVerticle
import io.vertx.core.Future
import io.vertx.ext.web.Router
import io.vertx.ext.web.handler.BodyHandler
import java.util.*

val random = Random(System.nanoTime())

class ControllerVerticle : AbstractVerticle() {

    override fun start(startFuture: Future<Void>) {
        // Create router
        val r = Router.router(vertx)

        // Create GET route and add handler
        val get = r.get("/get")
        get.handler {
            it.response().end("[v_:get] hello")
        }

        // Create GET route and add handler
        val getRandom = r.get("/get-random")
        getRandom.handler {
            it.response().end("[v_:get-random] hello ${random.nextInt()}")
        }

        // Create POST route and add handler
        val post = r.post("/post")
        post.handler(BodyHandler.create())
        post.handler {
            val name = it.bodyAsJson?.getString("name")
            if (name == null) {
                it.response().setStatusCode(400).end()
            } else {
                it.response().end("[v_:post] hello $name")
            }
        }

        // Start server
        vertx.createHttpServer()
                .requestHandler { r.accept(it) }
                .listen(8080) {
                    if (it.succeeded()) {
                        startFuture.complete()
                        log.info("Server started")
                        println("Controller started")
                    } else {
                        startFuture.fail(it.cause())
                        println("Controller failed to start")
                    }
                }
    }
}
