package sample

import io.gatling.core.Predef._
import io.gatling.http.Predef._

import scala.concurrent.duration._
import scala.util.Random

class Shared(val scenarioName: String) {
  private val nameFeeder = Iterator.continually(Map("name" -> Random.alphanumeric.take(Random.nextInt(8) + 8).mkString))

  private val get = exec(http("get").get("/get"))

  private val getRandom = exec(http("get-random").get("/get-random"))

  private val post = exec(http("post").post("/post").body(StringBody("""{ "name": "${name}" }""")).asJSON)

  private val sharedScenario = {
    scenario(scenarioName)
      .feed(nameFeeder)
      .repeat(100) {
        exec(get, getRandom, get, get, getRandom, post)
      }
  }

  val scenarioSetUp = sharedScenario
    .inject(heavisideUsers(5000) over (120 second))
  //.throttle(reachRps(300) in (10 seconds), holdFor(120 seconds))
}
