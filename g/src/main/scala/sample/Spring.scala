package sample

import io.gatling.core.Predef._
import io.gatling.http.Predef._

class Spring extends Simulation {
  private val httpConf = http
    //.proxy(Proxy("dev-proxy", 9501))
    .baseURL("http://dev-mongodb101z.emagazine.dev.rsc.local:8081")

  private val shared = new Shared("spring")

  setUp(shared.scenarioSetUp).protocols(httpConf)
}
