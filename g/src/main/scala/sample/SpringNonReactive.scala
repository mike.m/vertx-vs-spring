package sample

import io.gatling.core.Predef._
import io.gatling.http.Predef._

class SpringNonReactive extends Simulation {
  private val httpConf = http
    //.proxy(Proxy("dev-proxy", 9501))
    .baseURL("http://dev-mongodb101z.emagazine.dev.rsc.local:8082")

  private val shared = new Shared("spring non reactive")

  setUp(shared.scenarioSetUp).protocols(httpConf)
}
