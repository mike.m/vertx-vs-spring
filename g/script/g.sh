#!/bin/bash
gatling.sh -sf ~/vertx-vs-spring/g/src/main/scala -s sample.Vertx -on v
pause 5
gatling.sh -sf ~/vertx-vs-spring/g/src/main/scala -s sample.Spring -on s
pause 5
gatling.sh -sf ~/vertx-vs-spring/g/src/main/scala -s sample.SpringNonReactive -on snr
