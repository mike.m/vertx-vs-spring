package com.example.demo

import org.springframework.http.HttpStatus
import org.springframework.web.bind.annotation.*
import java.util.*

val random = Random(System.nanoTime())

@RestController
class Controller {
    @GetMapping("get")
    fun get(): String {
        return "[s_:get] hello"
    }

    @GetMapping("get-random")
    fun getRandom(): String {
        return "[s_:get-random] hello ${random.nextInt()}"
    }

    @PostMapping("post")
    fun post(@RequestBody body: PostDto): String {
        val name = body.name ?: throw BadRequest()
        return "[s_:post] hello $name"
    }

    @ExceptionHandler(BadRequest::class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    fun badRequest() {
    }
}
