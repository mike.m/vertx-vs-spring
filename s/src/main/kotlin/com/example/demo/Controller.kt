package com.example.demo

import org.springframework.http.HttpStatus
import org.springframework.web.bind.annotation.*
import reactor.core.publisher.Mono
import java.util.*

val random = Random(System.nanoTime())

@RestController
class Controller {
    @GetMapping("get")
    fun get(): Mono<String> {
        return Mono.just("[s_:get] hello")
    }

    @GetMapping("get-random")
    fun getRandom(): Mono<String> {
        return Mono.just("[s_:get-random] hello ${random.nextInt()}")
    }

    @PostMapping("post")
    fun post(@RequestBody body: PostDto): Mono<String> {
        val name = body.name ?: throw BadRequest()
        return Mono.just("[s_:post] hello $name")
    }

    @ExceptionHandler(BadRequest::class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    fun badRequest() {
    }
}
